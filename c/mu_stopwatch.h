﻿ /***************************************************************************

   Copyright 2011 Essensium NV

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

****************************************************************************/
   
#ifdef _MU_STOPWATCH__
#define _MU_STOPWATCH__ 

/*assume INT_TYPE is native integer
  of the system */

#include <sys/types.h>
#include <sys/time.h>

typedef struct timeval mu_timeval;

/* subtract two time_t values
   This creates a new object
   if result is NULL
   returns NULL on error 
   Sanity of input is NOT checked */
time_t* mu_TimeSubtract(mu_timeval *plus, mu_timeval *minus, mu_timeval *result)
{
	mu_timeval *res;
	if( !(plus && minus))
		return NULL; /* invalid operands*/
	if(result)
		res=result;
	else
		res=malloc(sizeof(*res));
	
	if(!res)
		return NULL;
		
	if(minus->tv_usec > plus->tv_usec)
	{
		plus->tv_sec -= 1;
		plus->tv_usec += 1000000;
	}
	
	res->tv_sec = plus->tv_sec  - minus->tv_sec;
	res->tv_usec= plus->tv_usec - minus->tv_usec;
		
	return res;
}


typedef struct {
	INT_TYPE running; /* boolean */
	mu_timeval started; /* start stamp */
	mu_timeval elapsed; /* is this necessary ? */
} mu_StopWatch;

mu_StopWatch* mu_StopWatch_init(mu_StopWatch *p_sw)
{
	if(!p_sw)
		return NULL;
	memset(p_sw, 0, sizeof(*p_sw));
	return p_sw;
}

mu_StopWatch* mu_StopWatch_start(mu_StopWatch *p_sw)
{
	int err;
	if(!p_sw)
		return NULL;
	if(p_sw->running)
		return NULL; /* cannot start if already running */
	
	err=gettimeofday(&(p_sw->started), NULL);
	if(err)
		return NULL;
	return p_sw;
}

mu_StopWatch* mu_StopWatch_stop(mu_StopWatch *p_sw)
{
	int err;
	if(!p_sw)
		return NULL;
	if(!(p_sw->running))
		return NULL;
	mu_timeval now;
	err=gettimeofday(&now,NULL);
	if(err)
		return NULL;
	mu_TimeSubtract(&now, &(p_sw->start), &(p_sv->elapsed));
	return p_sw;
}

	
	
		


#endif /* _MU_STOPWATCH__ */
